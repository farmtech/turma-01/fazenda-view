import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Fazenda } from './fazenda';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FazendaService {

  constructor(private httpClient:HttpClient) { }

  save(fazenda: Fazenda): Observable<any>{
    return this.httpClient.post('http://localhost:3000/fazenda', fazenda);
  }

  get(): Observable<any>{
    return this.httpClient.get('http://localhost:3000/fazenda');
  }

  excluir(fazenda: Fazenda): Observable<any>{
    return this.httpClient.delete('http://localhost:3000/fazenda/' + fazenda.id);
  }

}
