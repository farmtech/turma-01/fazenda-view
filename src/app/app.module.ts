import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NbAccordionModule, NbButtonModule, NbCardModule, NbInputModule, NbLayoutModule, NbSidebarModule, NbSidebarService, NbThemeModule, NbTreeGridModule } from '@nebular/theme';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NbThemeModule.forRoot(),
    NgxMaskModule.forRoot(),
    NbLayoutModule,
    NbSidebarModule,
    NbButtonModule,
    NbCardModule,
    NbAccordionModule,
    NbInputModule,
    FormsModule,
    HttpClientModule,
    NbTreeGridModule,
  ],
  providers: [NbSidebarService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
