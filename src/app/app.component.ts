import { Component, OnInit } from '@angular/core';
import { NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { Fazenda } from './fazenda';
import { FazendaService } from './fazenda.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'fazenda-view';

  fazenda:Fazenda = new Fazenda();
  fazendas:Fazenda[] = [];

  constructor(private fazendaService: FazendaService){
  }

  ngOnInit(): void {
    this.listar();
  }

  listar(){
    this.fazendaService.get().subscribe(r => {
      this.fazendas = r;
    });
  }

  salvar(){
    this.fazendaService.save(this.fazenda).subscribe(r => {
      this.fazenda = new Fazenda();
    });
  }

  selecionar(fazenda: Fazenda){
    this.fazenda = fazenda;
  }

  excluir(fazenda: Fazenda){
    this.fazendaService.excluir(fazenda).subscribe(r=> {
      this.listar();
    });
  }

}
